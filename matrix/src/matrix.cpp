//============================================================================
// Name        : matrix.cpp
// Author      : shobhit
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
class matrix
{
private:
	int row;
	int col;
	int **arr;

public:
  matrix(void)
  {
	  this->row=0;
	  this->col=0;
	  this->arr= NULL;
  }
  matrix(int row, int col )
    {
  	  this->row= row;
  	  this->col= col;
  	  this->arr=new int*[row];
  	  }
  void AcceptRecord()
  {

	  for(int i=0; i< this->row; i++)
	  {
		   arr[i]=new int[col];
		  for(int j=0; j < this->col; j++)
		  {
			  cout<<"enter elements:";
			  cin>>this->arr[i][j];
		  }

	  }


  }
  void PrintRecord()
  {
	  for(int i=0; i< this->row; i++)
	  	  {
	  		  for(int j=0; j< this->col; j++)
	  		  {
	  			  cout<<this->arr[i][j]<<" ";
	  		  }
	  		 cout<<endl;
	  	  }
  }

 ~matrix()
    {
  	 for(int i=0; i< this->row; i++)
  	 {
  		 delete[] arr[i];
  	 }
  	 delete[] arr;
  	 arr=NULL;
    }

};


int main()
{
	int row, col;
	cout<<"enter no. of rows";
	cin>>row;
	cout<<"enter no. of columns";
	cin>>col;
	matrix m1(row,col);
	m1.AcceptRecord();
	m1.PrintRecord();

	return 0;
}
